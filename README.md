# Event Showcase



## Installation

### common Installation
1. composer install
2. setup db connection in settings.php
3. make sure your "config_sync_directory" is "../config/sync"
4. drush si --existing-config

### Installation based on ddev
1. ddev start
2. ddev composer install
3. make sure your "config_sync_directory" is "../config/sync"
4. ddev drush si --existing-config


## Usage
### Content translation
- System's default language is EN, the secondary language is DE.
- Interface text language detection will follow the URL prefix.
- The initial Content Translation UI will pre-populate the source content field values.

### Json artists source
- We introduced the custom field type/widget/formater to support the use of json files as the source of multiple choice options.
- The json file should be placed in /json folder within custom module "es_artists"
- Each artists field instance can be set up to use a separate json source via field settings "Field Storage" -> "Json file name"
- Support for "artist" option sorting with the help of contributed module "improved_muli_select" and customized logic

### Link target
We extended the link field to support adding link target attributes.

### Events overview page
- The EN overview page is reachable via /events-overview.
- The DE overview page is reachable via /de/events-overview.

### API endpoint in JSON format to list all published events
- The EN events list is reachable via /api/events.
- The DE events list is reachable via /de/api/events.
