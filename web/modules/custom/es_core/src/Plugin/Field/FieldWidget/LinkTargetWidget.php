<?php

namespace Drupal\es_core\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Plugin implementation of the 'link_target_widget' widget.
 *
 * @FieldWidget(
 *   id = "link_target_widget",
 *   label = @Translation("Link support target"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkTargetWidget extends LinkWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'default_target' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['default_target'] = [
      '#type' => 'radios',
      '#title' => $this->t('Default Target'),
      '#description' => $this->t('The default link target'),
      '#default_value' => $this->getSetting('default_target'),
      '#options' => $this->supportedTargets(),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if ($default_target = $this->getSetting('default_target')) {
      $this->t('Selected default link target: @target', [
        '@target' => $default_target,
      ]);
    }
    return $summary;
  }

  /**
   * Helper function to provide an array of target options.
   *
   * @return array
   *   The options to use as targets
   */
  public static function supportedTargets() {
    return [
      '_self' => t('Current window (_self)'),
      '_blank' => t('New window (_blank)'),
      'parent' => t('Parent window (_parent)'),
      'top' => t('Topmost window (_top)'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $item = $this->getLinkItem($items, $delta);
    $options = $item->get('options')->getValue();

    $default_value = !empty($options['attributes']['target']) ? $options['attributes']['target'] : $this->getSetting('default_target');
    $element['options']['attributes']['target'] = [
      '#type' => 'select',
      '#title' => $this->t('Select a target'),
      '#options' => ['' => $this->t('- None -')] + $this->supportedTargets(),
      '#default_value' => $default_value,
      '#description' => $this->t('Select a link behavior. <em>_self</em> will open the link in the current window. <em>_blank</em> will open the link in a new window or tab. <em>_parent</em> and <em>_top</em> will generally open in the same window or tab, but in some cases will open in a different window.'),
    ];
    return $element;
  }

  /**
   * Getting link items.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   Returning of field items.
   * @param string $delta
   *   Returning field delta with item.
   *
   * @return \Drupal\link\LinkItemInterface
   *   Returning link items inteface.
   */
  private function getLinkItem(FieldItemListInterface $items, $delta) {
    return $items[$delta];
  }

}
