<?php

namespace Drupal\es_artists;

/**
 * Defines the interface for JsonHandler.
 */
interface JsonHandlerInterface {

  /**
   * Load Json data as array from json file.
   *
   * @param string $json_file_name
   *   The json file name.
   *
   * @return array
   *   The loaded json data.
   */
  public function loadJsonData(string $json_file_name);

}
