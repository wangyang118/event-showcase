<?php

namespace Drupal\es_artists;

use Drupal\Core\Extension\ModuleExtensionList;

/**
 * Custom json handler.
 */
class JsonHandler implements JsonHandlerInterface {

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The loaded json data.
   *
   * @var array
   */
  protected $jsonData = [];

  /**
   * Constructs a new JsonHandler.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   */
  public function __construct(ModuleExtensionList $extension_list_module) {
    $this->moduleExtensionList = $extension_list_module;
  }

  /**
   * {@inheritdoc}
   */
  public function loadJsonData(string $json_file_name) {
    if (!isset($this->jsonData[$json_file_name])) {
      $json_file_path = $this->moduleExtensionList->getPath('es_artists') . '/json/' . $json_file_name;
      if (file_exists($json_file_path)) {
        $json_contents = file_get_contents($json_file_path);
        if ($json = json_decode($json_contents, TRUE)) {
          $data = [];
          foreach ($json as $item) {
            $data[$item['id']] = $item;
          }
          $this->jsonData[$json_file_name] = $data;
        }
      }
    }
    return $this->jsonData[$json_file_name] ?? [];
  }

}
