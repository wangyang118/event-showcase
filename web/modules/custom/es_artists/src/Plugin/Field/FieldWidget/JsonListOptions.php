<?php

namespace Drupal\es_artists\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'es_list_json_options' widget.
 *
 * @FieldWidget(
 *   id = "es_list_json_options",
 *   label = @Translation("Select list (Json file source)"),
 *   field_types = {
 *     "es_list_json_item",
 *   },
 *   multiple_values = TRUE
 * )
 */
class JsonListOptions extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $options = $this->getOptions($items->getEntity());

    // Move the selected options to the top of list.
    $top_options = [];
    if ($empty_label = $this->getEmptyLabel()) {
      $top_options = ['_none' => $empty_label] + $top_options;
      unset($options['_none']);
    }
    foreach ($this->getSelectedOptions($items) as $id) {
      $top_options[$id] = $options[$id];
      unset($options[$id]);
    }
    $options = $top_options + $options;
    $element['#options'] = $options;
    return $element;
  }

}
