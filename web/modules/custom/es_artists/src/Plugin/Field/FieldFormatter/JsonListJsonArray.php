<?php

namespace Drupal\es_artists\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\es_artists\JsonHandlerInterface;
use Drupal\rest_views\Plugin\Field\FieldFormatter\ListExportFormatter;
use Drupal\rest_views\SerializedData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'list_custom_json_file_array' formatter.
 *
 * @FieldFormatter(
 *   id = "es_list_json_array",
 *   label = @Translation("Array for serialization"),
 *   field_types = {
 *     "es_list_json_item",
 *   }
 * )
 */
class JsonListJsonArray extends ListExportFormatter {

  /**
   * The json handler.
   *
   * @var \Drupal\es_artists\JsonHandlerInterface
   */
  protected $jsonHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, JsonHandlerInterface $json_hanlder) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->jsonHandler = $json_hanlder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('es_artists.json_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $exportFormat = $this->getSetting('export_format');

    // Only collect allowed options if there are actually items to display.
    if ($items->count()) {
      $provider = $items->getFieldDefinition()
        ->getFieldStorageDefinition()
        ->getOptionsProvider('value', $items->getEntity());
      // Flatten the possible options, to support opt groups.
      $options = OptGroup::flattenOptions($provider->getPossibleOptions());
      $json_data = [];
      if ($json_file_name = $this->getFieldSetting('json_file_name')) {
        $json_data = $this->jsonHandler->loadJsonData($json_file_name);
      }

      foreach ($items as $delta => $item) {
        $id = $item->value;
        $label = $options[$id] ?? '';
        if ($exportFormat == self::VALUE_LABEL) {
          $data = [
            'id' => $id,
            'name' => $label,
          ];
          if (isset($json_data[$id])) {
            foreach ($json_data[$id] as $key => $value) {
              if (in_array($key, ['id', 'name'])) {
                continue;
              }
              $data[$key] = $value;
            }
          }
        }
        else {
          $data = [$id => $label];
        }
        $elements[$delta] = [
          '#type' => 'data',
          '#data' => SerializedData::create($data),
        ];
      }
    }
    return $elements;
  }

}
