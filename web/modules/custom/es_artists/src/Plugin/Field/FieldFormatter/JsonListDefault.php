<?php

namespace Drupal\es_artists\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\es_artists\JsonHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'es_list_json_default' formatter.
 *
 * @FieldFormatter(
 *   id = "es_list_json_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "es_list_json_item",
 *   }
 * )
 */
class JsonListDefault extends FormatterBase {

  /**
   * The json handler.
   *
   * @var \Drupal\es_artists\JsonHandlerInterface
   */
  protected $jsonHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, JsonHandlerInterface $json_hanlder) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->jsonHandler = $json_hanlder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('es_artists.json_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    if ($items->count()) {
      // Only display exiting json item.
      if ($json_file_name = $this->getFieldSetting('json_file_name')) {
        $data = $this->jsonHandler->loadJsonData($json_file_name);
        foreach ($items as $delta => $item) {
          if (isset($data[$item->value]) && !empty($data[$item->value]['name'])) {
            $output = $data[$item->value]['name'] . ' (' . $item->value . ')';
            $elements[$delta] = [
              '#markup' => $output,
              '#allowed_tags' => FieldFilteredMarkup::allowedTags(),
            ];
          }
        }
      }
    }

    return $elements;
  }

}
