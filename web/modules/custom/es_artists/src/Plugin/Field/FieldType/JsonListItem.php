<?php

namespace Drupal\es_artists\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\options\Plugin\Field\FieldType\ListIntegerItem;

/**
 * Plugin implementation of the 'es_list_json_item' field type.
 *
 * @FieldType(
 *   id = "es_list_json_item",
 *   label = @Translation("List (Json file source)"),
 *   description = @Translation("This field stores values from a formatted json"),
 *   category = @Translation("Custom"),
 *   default_widget = "es_list_json_options",
 *   default_formatter = "es_list_json_default",
 * )
 */
class JsonListItem extends ListIntegerItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'json_file_name' => '',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $json_file_name = $this->getSetting('json_file_name');
    $allowed_values_function = $this->getSetting('allowed_values_function');

    $element['json_file_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Json file name'),
      '#default_value' => $json_file_name,
      '#access' => empty($allowed_values_function),
    ];

    $element['json_file_name']['#description'] = $this->t('The json file should be placed in /json folder under custom module es_artists.');

    $element['allowed_values_function'] = [
      '#type' => 'item',
      '#title' => $this->t('Allowed values list'),
      '#markup' => $this->t('The value of this field is being determined by the %function function and may not be changed.', ['%function' => $allowed_values_function]),
      '#access' => !empty($allowed_values_function),
      '#value' => $allowed_values_function,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(AccountInterface $account = NULL) {
    $allowed_options = [];
    foreach ($this->getJsonData() as $item) {
      $allowed_options[$item['id']] = $item['name'];
    }
    return $allowed_options;
  }

  /**
   * Load json data.
   */
  private function getJsonData() {
    if ($json_file_name = $this->getSetting('json_file_name')) {
      return \Drupal::service('es_artists.json_handler')->loadJsonData($json_file_name);
    }
    return [];
  }

}
